﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LVL2_ASPNet_MVC_01.Controllers
{
    public class CodingIdController : Controller
    {
        // GET: CodingId
        [Route("CodingId")]
        [HttpGet]
        public ActionResult Index()
        {
            string html = "<form method='post'>" +
                "<input type='text' name='name' />" +
                "<input type='submit' value='Greet Me' />" +
                "</form>";
            return Content(html, "text/html");
            //return View();
        }

        [Route("CodingId")]
        [HttpPost]
        public ActionResult Display(string name)
        {
            return Content(String.Format("<h1>Hello " + name + "</h1>"), "text/html");
        }

        [Route("CodingId/Aloha")]
        public ActionResult GoodBye()
        {
            //return Content("GoodBye");
            return View();
        }
    }
}